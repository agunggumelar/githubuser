package id.mpuji.githubuser

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import id.mpuji.githubuser.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_USER = "extra_user"
    }

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val user = intent.getParcelableExtra<User>(EXTRA_USER) as User

        val actionBar = supportActionBar
        actionBar!!.title = "${user.name}"
        actionBar.setDisplayHomeAsUpEnabled(true)

        Glide.with(this)
            .load(user.avatar)
            .into(binding.ivAvatar)
        binding.tvName.text = user.name
        binding.tvUsername.text = user.username
        binding.tvCompany.text = user.company
        binding.tvLocation.text = user.location
        binding.tvRepository.text = "• Repository ${user.repository}"
        binding.tvFollowers.text = "• Followers ${user.followers}"
        binding.tvFollowing.text = "• Following ${user.following}"
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}